// Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos.

function sumar_digitos(numero){

    if (numero >= 10 && numero < 100 ){
        const numero_1 = parseInt(numero / 10);
        const numero_2 = parseInt(numero % 10);
        const suma = numero_1 + numero_2;    
        return suma;
    }else{
        return 0
    }

}


module.exports = sumar_digitos;


