// Leer un número entero y determinar a cuánto es igual la suma de sus digitos.


let suma = 0;
function suma_digitos(numero){
    if (numero > 0) {
    
        let array = numero.toString().split('').map( digito => {
            suma = suma + parseInt(digito)
         } );
         return suma;
    
    } else {
       return 'Ingrese un numero valido';
    }
}

module.exports = suma_digitos;

