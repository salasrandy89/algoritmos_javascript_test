// Leer un número entero y mostrar en pantalla su tabla de multiplicar

let ultima_multiplicacion = 0;
function tabla_multiplicar(numero){
    for (let j = 1; j < 10 + 1; j++) {
       // console.log(` ${numero} x ${j} = ${numero*j}`);
       ultima_multiplicacion = numero*j;
      }
    
    return ultima_multiplicacion;

}

module.exports = tabla_multiplicar;

