// Leer un número entero de dos digitos y determinar si los dos digitos son iguales.

function validar_digitos_iguales(numero){
    if (numero >= 10 && numero < 100 ){
        const numero_1 = parseInt(numero / 10);
        const numero_2 = parseInt(numero % 10);
        if ( numero_1 == numero_2 ){
            return true;
        } else {
            return false;
        }
    } else{
        return 'error';
    }
}

module.exports = validar_digitos_iguales;

