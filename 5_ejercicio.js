// Leer un número entena de dos digitos y determinar si es primo y además si es negativo.
function validar_numero_primo(numero) {
    
    if ( numero > 10 && numero < 100 ){
    
        for( let i = 2; i < numero ; i++ ){
            if ( ( numero % i) == 0 ){
                return false;
            }
        }
        return true;
        
    } else if (numero < 0){
        return 'negativo';
    }
    
}

module.exports = validar_numero_primo;
