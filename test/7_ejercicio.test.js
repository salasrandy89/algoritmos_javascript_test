const getdata = require('../7_ejercicio');

test('Leer un número entero de dos digitos y determinar si los dos digitos son iguales.', () => {
    expect(getdata(22)).toBe(true);
});