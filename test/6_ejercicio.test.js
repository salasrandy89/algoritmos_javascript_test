const getdata = require('../6_ejercicio');

test('Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.', () => {
    expect(getdata(42)).toBe('El primer digito es multiplo del segundo');
});