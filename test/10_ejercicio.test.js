const getdata = require('../10_ejercicio');

test('Leer dos numeros enteros y determinar si la diferencia es un divisor exacto de alguno de lo dos numeros. ', () => {
    expect(getdata(28,21)).toBe('divisor exacto de 28');
});