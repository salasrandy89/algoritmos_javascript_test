const getdata = require('../8_ejercicio');

test('Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo. ', () => {
    expect(getdata(3118)).toBe(true);
});