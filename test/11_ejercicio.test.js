const getdata = require('../11_ejercicio');

test('Leer un número entero y determinar a cuánto es igual la suma de sus digitos.', () => {
    expect(getdata(1111)).toBe(4);
});