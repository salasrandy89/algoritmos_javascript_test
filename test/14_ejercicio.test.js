const getdata = require('../14_ejercicio');

test('Leer un número entero y mostrar en pantalla su tabla de multiplicar', () => {
    expect(getdata(5)).toBe(50);
});