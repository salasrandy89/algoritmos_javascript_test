const getdata = require('../4_ejercicio');

test('Validar un número entero de dos digitos menor que 20 y determinar si es primo.', () => {
    expect(getdata(13)).toBe(true);
});