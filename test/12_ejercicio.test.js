const getdata = require('../12_ejercicio');

test('Leer un número entero y determinar cuantas veces tiene el digito 1', () => {
    expect(getdata(1111)).toBe(4);
});