const getsumar_digitos = require('../2_ejercicio');

test('Sumar digitos de un numero', () => {
    expect(getsumar_digitos(23)).toBe(5);
});