// Leer un número entero de dos digitos menor que 20 y determinar si es primo.

function numero_primo(numero){

    if ( numero > 10 && numero < 20 ){

        for( let i = 2; i < numero; i++ ){
            if ( ( numero % i) == 0 ){
                return false;
            }
        }
        return true;
    
    } else {
        return false;
    }
}

module.exports = numero_primo;

