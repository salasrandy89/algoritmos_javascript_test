// Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.

function validar_si_son_multiplos(numero){

    if (numero >= 10 && numero < 100 ){
        const numero_1 = parseInt(numero / 10);
        const numero_2 = parseInt(numero % 10);
    
        if ( (numero_1 % numero_2) == 0 ){
            return 'El primer digito es multiplo del segundo';
        }
    
        if ( (numero_2 % numero_1) == 0 ){
            return 'El segundo digito es multiplo del primero';
        }
    
    } else{
        return 'Ingrese un numero valido';
    }

}

module.exports = validar_si_son_multiplos;



