// Leer un número entero de dos digitos y determinar si ambos digitos son pares

function validar_dos_digitos_pares(numero){

    if (numero >= 10 && numero < 100 ){
        const numero_1 = parseInt(numero / 10);
        const numero_2 = parseInt(numero % 10);
        const suma = numero_1 + numero_2;   

        if ( (numero_1 % 2) == 0 && (numero_2 % 2) == 0 ){
            return true;
        }else{
            return false;
        }
    
    }else{
        return false;
    }

}


module.exports = validar_dos_digitos_pares;


