// Leer 10 números enteros, almacenados en un vcetor y 
// determinar cuántas veces está recetido el mayor


numero_1   = 11;
numero_2   = 1;
numero_3   = 13;
numero_4   = 14;
numero_5   = 16;
numero_6   = 122;
numero_7   = 122;
numero_8   = 16;
numero_9   = 18;
numero_10  = 119;

let vector = [];
let numero_mayor = 0;
let contador = 0;

function llenar_vector(){
  vector.push(numero_1);
  vector.push(numero_2);
  vector.push(numero_3);
  vector.push(numero_4);
  vector.push(numero_5);
  vector.push(numero_6);
  vector.push(numero_7);
  vector.push(numero_8);
  vector.push(numero_9);
  vector.push(numero_10);
}

function mayor_repetido(){

  llenar_vector();
  
  let buscar_mayor = vector.map(buscar => {
    if( buscar > numero_mayor ){
        numero_mayor = buscar;
    }
  });

  let buscar_veces_repetido = vector.map(numeros => {
        if( numeros == numero_mayor ){
            contador += 1;
        }
  });


    return contador;
}

module.exports = mayor_repetido;

