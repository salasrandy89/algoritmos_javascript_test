// Leer 10 números enteros, almacenarlos en un vector y determinar
// en qué posiciones se encuentran los números terminados en O


let vector = [];
let posicion_terminados_cero = [];

numero_1   = 11;
numero_2   = 1;
numero_3   = 130;
numero_4   = 14;
numero_5   = 16;
numero_6   = 122;
numero_7   = 13;
numero_8   = 16;
numero_9   = 180;
numero_10  = 19;

function llenar_vector(){
  vector.push(numero_1);
  vector.push(numero_2);
  vector.push(numero_3);
  vector.push(numero_4);
  vector.push(numero_5);
  vector.push(numero_6);
  vector.push(numero_7);
  vector.push(numero_8);
  vector.push(numero_9);
  vector.push(numero_10);
}

function contar_terminados_cero(){

  llenar_vector();
  vector.forEach( (numero, index) => {
      if( (numero % 2) == 0  & (numero % 5)== 0 ) {
          posicion_terminados_cero.push(index)
      } 
    });
    
   // console.log(`Total de numero terminados en cero : ${posicion_terminados_cero.length}`);

    // if( posicion_terminados_cero.length > 0){
    //     posicion_terminados_cero.forEach(posicion => console.log(`Posicion ${posicion}`));
    // }

    return posicion_terminados_cero.length;
}








module.exports = contar_terminados_cero;

