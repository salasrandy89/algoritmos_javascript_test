// Leer dos numeres enteros y determinar si la diferencia entre  
// los dos es un número divisor exacto de alguno de los dos números.

const numero_1 = 28;
const numero_2 = 21;

function divisor_exacto(numero_1, numero_2) {

    const diferencia = numero_1 - numero_2;
    if ( (numero_1 % diferencia) == 0 ) {
        return `divisor exacto de ${numero_1}` ;

    } 
    if ( (numero_2 % diferencia) == 0 )  {
        return `divisor exacto de ${numero_2}` ;
    } 
}

module.exports = divisor_exacto;
