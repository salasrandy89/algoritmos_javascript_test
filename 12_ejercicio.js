// Leer un número entero y determinar cuantas veces tiene el digito 1

let contador = 0;
function numero_veces_repetido(numero){
    if (numero > 0) {

        let array = numero.toString().split('').map( digito => {
            if ( parseInt(digito) == 1){
                contador += 1
            }
        } );
        return contador;

    } else {
    console.log('Error')
    }

}

module.exports = numero_veces_repetido;

